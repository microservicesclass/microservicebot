from fastapi import FastAPI
from api.database import database, engine
from api.routers import router
from api import models
import os

os.environ["OPENAI_API_KEY"] = "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"

app = FastAPI()

app.include_router(router)


def create_tables():
    models.Base.metadata.create_all(bind=engine)


@app.on_event("startup")
async def startup():
    create_tables()
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host='0.0.0.0', reload=True)
