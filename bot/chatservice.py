import httpx
from bot.init_llm import LLMChainInitializer
from bot.chat_manager import ChatManager

USER_SERVICE_URL = "http://localhost:8001"  # Replace with the actual URL of your user service


class BotService:
    @staticmethod
    def user_exists(username: str) -> bool:
        """Check if a user exists in the user CRUD service by their username."""
        response = httpx.get(f"{USER_SERVICE_URL}/users/username/{username}")
        return response.status_code == 200

    @staticmethod
    def make_prediction(username: str, human_input: str):
        # Verify if the user exists by their username
        if not BotService.user_exists(username):
            raise ValueError("User does not exist in the user service.")

        llm_chain = LLMChainInitializer.get_llm_chain()

        # Fetch chat history for the user
        chat_history = ChatManager.get_chat_history(user_id=username)  # Assuming ChatManager uses username as user_id

        # Use chat history with your llm_chain.predict if needed
        response = llm_chain.predict(human_input=human_input, chat_history=chat_history)

        # Update chat history in the database
        ChatManager.update_chat_history(username, {"human": human_input, "response": response})

        return response