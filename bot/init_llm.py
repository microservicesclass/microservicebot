from langchain import LLMChain
from langchain.memory import ConversationBufferMemory
from langchain.chat_models import ChatOpenAI
from bot.prompt import llm_prompt


class LLMChainInitializer:
    _llm_chain = None

    @classmethod
    def get_llm_chain(cls):
        if not cls._llm_chain:
            llm_model = ChatOpenAI()
            memory = ConversationBufferMemory(memory_key="chat_history", human_prefix="Humano", ai_prefix="IA")
            cls._llm_chain = LLMChain(llm=llm_model, prompt=llm_prompt, verbose=True, memory=memory)
        return cls._llm_chain
