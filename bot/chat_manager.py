from api.database import SessionLocal
from api.models import User, Conversation


class ChatManager:
    @staticmethod
    def get_chat_history(user_id: str):
        session = SessionLocal()
        user = session.query(User).filter(User.id == user_id).first()
        if user:
            return user.conversations
        return []

    @staticmethod
    def update_chat_history(user_id: str, chat_data: dict):
        session = SessionLocal()
        user = session.query(User).filter(User.id == user_id).first()
        if user:
            conversation = Conversation(user_id=user.id, text=chat_data["human"])
            session.add(conversation)
            session.commit()
        session.close()
