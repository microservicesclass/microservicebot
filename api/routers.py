from fastapi import APIRouter, Depends

from bot.chatservice import BotService  # Import the PredictService

router = APIRouter()


@router.post("/predict/")
async def predict(username: str, text: str):
    # Use the PredictService to make the prediction and handle database operations
    prediction = BotService.make_prediction(username=username, human_input=text)
    return {"prediction": prediction}
