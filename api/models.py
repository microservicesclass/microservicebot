from dataclasses import dataclass
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, declarative_base

Base = declarative_base()


@dataclass
class User(Base):
    __tablename__ = "users"
    id: int
    username: str
    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    conversations = relationship("Conversation", back_populates="user")


@dataclass
class Conversation(Base):
    __tablename__ = "conversations"
    id: int
    user_id: int
    text: str
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id"))
    text = Column(String)
    user = relationship("User", back_populates="conversations")
