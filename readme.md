# Microservice Bot
Author: Gustavo Aquino 

The `microservicebot` is a FastAPI based microservice designed to manage user data and provide user verification for a chatbot service. It allows for the creation, reading, updating, and deletion of user data, and can be used to verify user existence by username.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [API Endpoints](#api-endpoints)
- [Contributing](#contributing)
- [License](#license)

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/microservicesclass/microservicebot.git

2. cd microservicebot

3. Create an environment
    ```bash
    conda create -n microservicebot python=3.10
    
4. conda activate microservicebot
    
5. Install the requirements:
    ```bash
   pip install -r requirements.txt

6. python main.py

This will start the FastAPI application on http://127.0.0.1:8000/docs.